/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isoreqestsend.utility;

/**
 *
 * @author Andri D. Septian
 */
public class Constants {

    public static final String HTTP_SERVLET_ENTITY_AUTH = "x-api-key";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_IDENTIFIER = "user-agent";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_AUTH = "authorization";

    public static final String REQ_BODY = "REQ_BODY";
    public static final String BODY_RESPONSE = "BODY_RESPONSE";
    public static final String PATH = "PATH";
    public static final String LOG = "LOG";
    public static final String ABORTED_RESPONSE = "ABORTED_RESPONSE";
    public static final String STATUS = "STATUS";

    public class CONTROLLER {

        public static final String GROUP = "GROUP";
    }

    public class MODEL {

        public static final String MUSER = "MUSER";
        public static final String LOGMOBILEACTIVITY = "LOGMOBILEACTIVITY";

        public class WEB {

            public static final String AAUTHUSERS = "AAUTHUSERS";
            public static final String KARYAWAN = "KARYAWAN";
        }
    }

    public class MS {

        public static final String REQUEST_BODY = "REQUEST_BODY";
        public static final String PATH = "PATH";
        public static final String PARTNER_CORE = "PARTNER_CORE";
        public static final String TRXMGR = "TRXMGR";
        public static final String TIMEOUT = "TIMEOUT";
        public static final String REQUEST = "REQUEST";
        public static final String RESPONSE = "RESPONSE";
        public static final String OTP = "OTP";
        public static final String OTP_EN = "OTP_EN";
        public static final String MSISDN = "MSISDN";
        public static final String DESTINATION = "DESTINATION";
        public static final String TOKEN = "TOKEN";
        public static final String LOG_TX = "LOG_TX";
        public static final String INFO = "INFO";

        public class STATUS {

            public static final String SUCCESS = "0";
            public static final String FAILED = "1";
        }

        public class RC {

            public static final String SUCCESS = "00";
            public static final String USER_AUTH_FAILED = "10";
            public static final String USER_NOT_FOUND = "11";
            public static final String PIN_AUTH_FAILED = "12";
            public static final String PIN_MISMATCH = "13";
            public static final String MSISDN_ALREADY_EXIST = "14";
            public static final String USER_NOT_ACTIVE = "15";
            public static final String USER_MSISDN_ALREADY_EXIST = "16";
            public static final String USER_CIF_ALREADY_EXIST = "17";
            public static final String DATA_NOT_FOUND = "20";
            public static final String UNREGISTERED_ACCOUNT_NUMBER = "21";
            public static final String BALANCE_NOT_SUFFICIENT = "30";
            public static final String TIMEOUT = "50";
            public static final String PARTNER_NOT_FOUND = "60";
            public static final String FAILED_FROM_CORE = "70";
            public static final String OTP_FAILED = "97";
            public static final String AUTH_FAILED = "98";
            public static final String SERVICE_PATH_NOT_FOUND = "99";
            public static final String INTERNAL_ERROR = "IE";
            public static final String FAILED = "FA";
        }
    }

    public static class TRANSACTIONAL_MANAGER {

        public static final String GROUP = "GROUP";

    }

    public static class CONSOLE {

        public static final String ANSI_GREEN = "\u001B[32m";
        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_RED = "\u001B[31m";
    }

}
