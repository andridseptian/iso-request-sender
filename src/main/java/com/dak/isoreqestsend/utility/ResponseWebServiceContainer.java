package com.dak.isoreqestsend.utility;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author risyamaulana
 */
public class ResponseWebServiceContainer {

    public final String RC = "rc";
    public final String RM = "rm";
    private JSONObject json;

    public ResponseWebServiceContainer() {

    }

    public ResponseWebServiceContainer(JSONObject jObjectResponse) {
        json = jObjectResponse;
    }

    public ResponseWebServiceContainer(String rc, String desc) {
        json = new JSONObject().put(RC, rc).put(RM, desc);
    }

    public ResponseWebServiceContainer(String rc, String desc, JSONObject another) {
//        json = Utility.merge(new JSONObject().put(RC, rc).put(RM, desc), another);
        JSONObject response = new JSONObject();
        response.put("rc", rc);
        response.put("rm", desc);
        response.put("data", another);
        json = response;
    }
    
    public ResponseWebServiceContainer(String rc, String desc, JSONArray another) {
//        json = Utility.merge(new JSONObject().put(RC, rc).put(RM, desc), another);
        JSONObject response = new JSONObject();
        response.put("rc", rc);
        response.put("rm", desc);
        response.put("data", another);
        json = response;
    }

    public JSONObject toJSONObject() {
        return json;
    }

    public String jsonToString() {
        return json.toString();
    }

}
