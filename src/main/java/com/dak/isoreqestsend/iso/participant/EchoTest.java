package com.dak.isoreqestsend.iso.participant;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.hibernate.HibernateException;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;

/**
 *
 * @author risyamaulana
 */
public class EchoTest implements Runnable, Configurable {

    Log log = Log.getLog("Q2", getClass().getName());
    Configuration cfg;

    SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");

    public EchoTest() {
        super();
    }

    @Override
    public void run() {
        checkConnected();
    }

    private void checkConnected() {

        String host = cfg.get("host");
        
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        String stan =  s.substring(s.length() - 6);
        String time = sdf.format(new Date());

        try {
            BaseChannel chn = (BaseChannel) NameRegistrar.get("channel." + host);
//            if (chn.isConnected()) {
                MUX mux = (MUX) NameRegistrar.get("mux." + host + "-mux");
                ISOMsg r = new ISOMsg();
                r.setPackager(new ISO87APackager());
                r.setMTI("0800");
                r.set(7, time);
                r.set(11, stan);
                r.set(70, "301");

                log.info("Sending request to " + host, r);
                log.info(r.getPackager());
                log.info(r.pack().toString());
                ISOMsg rsp = mux.request(r, 60000);
                
                
                if (rsp != null) {
                    log.info("Received response from " + host, rsp);
                    log.info("Connection test to " + host + " success");
                } else {
                    log.info("Connection test to " + host + " timeout");
                }
//            }
        } catch (ISOException ex) {
            log.error(ex);
        } catch (NameRegistrar.NotFoundException ex) {
            log.error(ex);
        } catch (HibernateException ex) {
            log.error(ex);
        }

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        cfg = c;
    }

}
