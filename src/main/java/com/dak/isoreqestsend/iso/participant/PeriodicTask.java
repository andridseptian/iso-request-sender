/*
 * jPOS Project [http://jpos.org]
 * Copyright (C) 2000-2009 Alejandro P. Revilla
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.dak.isoreqestsend.iso.participant;

import org.jdom.Element;
import org.jpos.core.Configurable;
import org.jpos.iso.ISOUtil;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.QFactory;
import org.jpos.util.NameRegistrar;

/**
 * DailyTask Adaptor
 *
 * @author Alejandro Revilla
 * @version $Revision: 2776 $ $Date: 2009-09-18 22:53:31 -0300 (Fri, 18 Sep 2009) $
 */
public class PeriodicTask extends QBeanSupport implements Runnable {
    int delay;
    Runnable task;
    Thread thisThread = null;

    public PeriodicTask() {
        super ();
    }

    @Override
    protected void initService () throws Exception {
        QFactory factory = getServer().getFactory();
        Element e = getPersist ();
        delay = Integer.parseInt(e.getChildTextTrim("delay"));
        task = (Runnable) factory.newInstance (e.getChildTextTrim("class"));
        factory.setLogger (task, e);
    }
    @Override
    public void startService () throws Exception {
        NameRegistrar.register(getName(), this);
        if (task instanceof Configurable) {
            Element e = getPersist ();
            QFactory factory = getServer().getFactory();
            ((Configurable)task).setConfiguration (
                factory.getConfiguration (e)
            );
        }
        QFactory.invoke(task, "setServer", getServer());
        (thisThread = new Thread(this)).start();
    }
    @Override
    public void stopService () throws Exception {
        NameRegistrar.unregister(getName());
        if (thisThread != null)
            thisThread.interrupt();
    }

    /**
     * Execute script every certain period
     *
     */
    @Override
    public void run () {
        int d = 0;
        while (running()) {
            if (d <= 0) {
                if (running()) {
                    try {
                        getServer().getFactory().invoke(task, "setServer", getServer());
                    } catch (Exception e) {
                    }
                    Thread taskThread = new Thread(task);
                    taskThread.setDaemon (true);
                    taskThread.start();
                    ISOUtil.sleep (1000);
                }
                d = delay;
            }
            d--;
            ISOUtil.sleep (1000);
        }
    }

}