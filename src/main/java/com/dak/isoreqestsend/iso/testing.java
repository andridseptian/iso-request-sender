/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isoreqestsend.iso;

import com.dak.isoreqestsend.utility.Constants;
import com.dak.isoreqestsend.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.MUX;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.jpos.util.NameRegistrar;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class testing implements TransactionParticipant, Configurable {

    Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    ResponseWebServiceContainer msResponse = new ResponseWebServiceContainer();

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        String stan = s.substring(s.length() - 6);
        String time = sdf.format(new Date());

        JSONObject reqBody = new JSONObject();

        System.out.println("sebeulm try");

        try {
//            String host = cfg.get("host", "xwdak");
            String host = "xwdak";

            System.out.println("setelah host");

            BaseChannel chn = (BaseChannel) NameRegistrar.get("channel." + host);
            MUX mux = (MUX) NameRegistrar.get("mux." + host + "-mux");

            System.out.println("setelah mux");

            ISOMsg r = new ISOMsg();
            r.setPackager(new ISO87APackager());
            r.setMTI("0800");
            r.set(7, time); //Transmission date & time MMDDhhmmss
            r.set(11, stan); //system trace audit number (stan)
            r.set(46, "ini adalah testing");
            r.pack();
            
            log.info("Sending request to " + host, r);
            log.info("Get Package : " + r.getPackager().getDescription());
            log.info("Get pack : " + r.pack());
            ISOMsg rsp = mux.request(r, 60000);

            reqBody.put("data", "bisa");
            log.info("SUKSES");
            String rc = "00";
            String rm = "SUKSES";
            msResponse = new ResponseWebServiceContainer(rc, rm, reqBody);
            ctx.put(Constants.BODY_RESPONSE, msResponse);
            return PREPARED | NO_JOIN;

        } catch (Exception e) {
            log.info("ERROR: " + e);
            reqBody.put("data", "tidak");
            String rc = "99";
            String rm = "Internal Error";
            msResponse = new ResponseWebServiceContainer(rc, rm, reqBody);
            ctx.put(Constants.BODY_RESPONSE, msResponse);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void commit(long l, Serializable srlzbl) {

    }

    @Override
    public void abort(long l, Serializable srlzbl) {

    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {

    }

}
