/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isoreqestsend.controller;

import com.dak.isoreqestsend.model.LogActivity;
import com.dak.isoreqestsend.spring.Spring;
import com.dak.isoreqestsend.utility.Constants;
import com.dak.isoreqestsend.utility.ResponseWebServiceContainer;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andri D Septian
 */
@RestController
public class ISOController {

    Log log = Log.getLog("Q2", getClass().getName());
    ResponseWebServiceContainer msResponse = new ResponseWebServiceContainer();

    @RequestMapping(value = "/iso/dev/{service_type}", method = RequestMethod.POST)
    public String allProcess(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {

        LogActivity logActivity = new LogActivity();
        JSONObject json = new JSONObject(body);

        try {
            System.out.println("-->REQ BODY CONTROLLER : " + body);
            Context ctx = new Context();
            Space space = SpaceFactory.getSpace();

            logActivity.setRequest(body.toString());
            logActivity.setRequestTime(new Date());
            logActivity.setRequestUrl(path);

            logActivity = Spring.getLogActivityDao().saveOrUpdate(logActivity);

            ctx.put(Constants.REQ_BODY, json);
            ctx.put(Constants.PATH, path);
            ctx.put(Constants.LOG, logActivity);

            int timeout = 60000;

            space.out("iso-txn", ctx, timeout);
            Context ctx_response = (Context) space.in(logActivity.getId(), timeout);

            msResponse = (ResponseWebServiceContainer) ctx_response.get(Constants.BODY_RESPONSE);

            json.put("status", "sukses");
            log.info("->RESPONSE BODY CONTROLLER : " + json);
//            return msResponse.jsonToString();
            return json.toString();
        } catch (JSONException e) {
            log.info(e);
            msResponse = new ResponseWebServiceContainer(new JSONObject().put("response", "gagal"));
            return msResponse.jsonToString();
        }

    }
}
