/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isoreqestsend.main;

import com.dak.isoreqestsend.spring.Spring;
import javax.naming.ConfigurationException;
import org.jpos.q2.Q2;
import org.jpos.util.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Andri D Septian
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.dak.isoreqestsend.controller"})
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public class Main {

    Log log = Log.getLog("Q2", getClass().getName());

    public static void main(String[] args) {
        try {
            SpringApplication.run(Main.class, args);
            Spring spring = new Spring();
//            Q2 q2 = new Q2("deploy");
            Q2 q2 = new Q2("src/main/resources/deploy");
            q2.start();
            spring.initService();
        } catch (ConfigurationException ce) {
            ce.printStackTrace();
        }
    }

}
