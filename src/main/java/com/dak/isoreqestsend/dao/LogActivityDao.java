/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.isoreqestsend.dao;

import com.dak.isoreqestsend.model.LogActivity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "LogActivityDao")
@Transactional(value = "transactionManagerInternal")
public class LogActivityDao extends Dao {

    public LogActivity saveOrUpdate(LogActivity logActivity) {
        if (logActivity.getId() == null) {
            em.persist(logActivity);
            System.out.println("persist / saved");
        } else {
            em.merge(logActivity);
            System.out.println("Merge / updated");
        }
        return logActivity;
    }
}
